using System.Collections.Generic;
using System.Linq;
using DrinkStore.Models;

namespace DrinkStore.Data
{
    public class Seeder
    {
        private ApplicationDbContext _context;
        public Seeder(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CreateDrinkCategory()
        {
            var categories = new List<Category>
            {
                new Category{Name="Alcolic"},
                new Category{Name="Non Alcolic"}
            };

            if(_context.Categories.Any()){
                return;
            }
            _context.Categories.AddRange(categories);
            _context.SaveChanges();
        }
    }
}