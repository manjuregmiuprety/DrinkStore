using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DrinkStore.Models;
using DrinkStore.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DrinkStore.Controllers
{
    public class DrinkController : Controller
    {
        private IDrinkRepository _repository;
        private ICategoryRepository _categoryRepository;
        public DrinkController(IDrinkRepository repository, ICategoryRepository categoryRepository)
        {
            _repository = repository;
            _categoryRepository = categoryRepository;
        }
        public IActionResult Index(int drinkId, string name = null)
        {
            if(string.IsNullOrEmpty(name)){
                return View(_repository.GetAllDrinks());
            } else{
                return View(_repository.GetDrinkByCategory(name));
            }
        }
        [HttpGet]
        public IActionResult New()
        {
            var category = _repository.GetDrinkCategories();
            ViewBag.drinkCategory = new SelectList(category,nameof(Category.Id),nameof(Category.Name));


            return View();
        }
        [HttpPost]
        public IActionResult New(Drink drink)
        {
            _repository.Create(drink);
            return RedirectToAction("Index");
        }

        public IActionResult Details(int Id)
        {
            
            return View(_repository.GetSingleDrink(Id));
        }

    }
}