using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DrinkStore.Models;
using DrinkStore.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;

namespace DrinkStore.Controllers
{
    public class PurchaseController : Controller
    {
        private IPurchaseRepository  _purchaseRepository;
        private ICartRepository _cartRepository;
        private UserManager<ApplicationUser> _userManager;
        public PurchaseController(IPurchaseRepository purchaseRepository,
                                ICartRepository cartRepository,
                                UserManager<ApplicationUser> userManager)
        {
           _purchaseRepository = purchaseRepository;
           _cartRepository = cartRepository;
           _userManager = userManager; 
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult New()
        {
            var customerId = _userManager.GetUserId(HttpContext.User);
            var carts = _cartRepository.GetCustomerCart(customerId);

            var purchases = carts.Select(c => new Purchase{
                DrinkId = c.DrinkId,
                CustomerId = c.CustomerId,
                DrinkPrice = c.Drink.Price,
                DrinkName = c.Drink.Name
            }).ToList();

            // foreach(var cart in carts)
            // {
            //     var purchase = new Purchase();
            //     purchase.DrinkId = cart.DrinkId;
            //     purchase.CustomerId = cart.CustomerId;
            //     purchase.DrinkPrice= cart.Drink.Price;
            //     purchase.DrinkName = cart.Drink.Name;

            //     _purchaseRepository.create(purchase);
            // }
            _purchaseRepository.createPurchase(purchases);

            _cartRepository.DeleteCustomerCart(carts);
            return RedirectToAction(nameof(DrinkController.Index),"Drink");
        }
    }
}