using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DrinkStore.Models;
using DrinkStore.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using ClientNotifications;
using static ClientNotifications.Helpers.NotificationHelper;

namespace DrinkStore.Controllers
{
    public class CartController : Controller
    {
        private ICartRepository _cartRepository;
        private IDrinkRepository _drinkRepository;
        private UserManager<ApplicationUser> _userManger;
        private IClientNotification _clientNotification;
        public CartController(ICartRepository cartRepository,
                                IDrinkRepository drinkRepository,
                                UserManager<ApplicationUser> userManager,
                                IClientNotification clientNotification)
        {
            _cartRepository = cartRepository;
            _drinkRepository = drinkRepository;
            _userManger = userManager;
            _clientNotification = clientNotification;
        }
        public IActionResult Index()
        {
            var carts = _cartRepository.GetCustomerCart(_userManger.GetUserId(HttpContext.User));
            ViewBag.TotalPrice = carts.Sum(c=>c.Price * c.Quantity);
            return View(carts);
        }
        [HttpGet]
        public IActionResult New(int drinkId)
        {
            var drink = _drinkRepository.GetSingleDrink(drinkId);

           Cart cart = new Cart();
           cart.DrinkId = drink.Id;
           cart.Price = drink.Price;
           cart.Quantity = 1;
           cart.CustomerId = _userManger.GetUserId(HttpContext.User);
           _cartRepository.Create(cart);
           _clientNotification.AddToastNotification("New cart created",
                                            NotificationType.success,
                                            null);

            
            var cartCount = _cartRepository.GetCustomerCart(_userManger.GetUserId(HttpContext.User)).Count;
            return Ok(new {count = cartCount});
        }

        public IActionResult GetCartCount(){
            var cartCount = _cartRepository.GetCustomerCart(_userManger.GetUserId(HttpContext.User)).Count;
            return Ok(new {count = cartCount});
        }

        public IActionResult UpdateCartQuantity(int Id, int quantity){
            if(quantity<1){
                return BadRequest("Invalid Quantity");
            }
            var cart = _cartRepository.GetSingleCart(Id);
            cart.Quantity = quantity;

            _cartRepository.Update(cart);

            var carts = _cartRepository.GetCustomerCart(_userManger.GetUserId(HttpContext.User));
            var totalPrice = carts.Sum(c=>c.Price * c.Quantity);
            return Ok(new{TotalPrice = totalPrice});
        }

        public IActionResult Delete(int cartId){
            var cart = _cartRepository.GetSingleCart(cartId);
            _cartRepository.Delete(cart);
            return RedirectToAction("Index");
        }
    }
}