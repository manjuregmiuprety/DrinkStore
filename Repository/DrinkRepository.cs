using System.Collections.Generic;
using System.Linq;
using DrinkStore.Data;
using DrinkStore.Models;
using Microsoft.EntityFrameworkCore;

namespace DrinkStore.Repository
{
    public class DrinkRepository : IDrinkRepository
    {
        private ApplicationDbContext _context;
        public DrinkRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Drink> AddToCart(int Id)
        {
           return _context.Drinks.ToList();
        }

        public void Create(Drink drink)
        {
            _context.Drinks.Add(drink);
            _context.SaveChanges();
        }

        public void Delete(Drink drink)
        {
            _context.Drinks.Remove(drink);
            _context.SaveChanges();
        }

        public List<Category> GetAllDrinks()
        {
            return _context.Categories.Include(c=>c.Drinks).ToList();
        }

        public List<Category> GetDrinkByCategory(string name)
        {
            return _context.Categories
                            .Where(c=>c.Name.Equals(name))
                            .Include(c=>c.Drinks)
                            .ToList();
        }

        public List<Category> GetDrinkCategories()
        {
            return _context.Categories.ToList();
        }

        public Drink GetSingleDrink(int Id)
        {
            return _context.Drinks.FirstOrDefault(d=>d.Id==Id);
        }

        public void Update(Drink drink)
        {
            _context.Drinks.Update(drink);
            _context.SaveChanges();
        }
    }
}