using System.Collections.Generic;
using DrinkStore.Models;

namespace DrinkStore.Repository
{
    public interface IPurchaseRepository
    {
        void create(Purchase purchase);
        void createPurchase(List<Purchase> purchases);
    }
}