using System.Collections.Generic;
using DrinkStore.Data;
using DrinkStore.Models;

namespace DrinkStore.Repository
{

    public class PurchaseRepository : IPurchaseRepository
    {
        private ApplicationDbContext _context;
        public PurchaseRepository(ApplicationDbContext context)
        {
           _context = context; 
        }
        public void create(Purchase purchase)
        {
            _context.Purchases.Add(purchase);
            _context.SaveChanges();
        }

        public void createPurchase(List<Purchase> purchases)
        {
            _context.Purchases.AddRange(purchases);
            _context.SaveChanges();
        }
    }
}