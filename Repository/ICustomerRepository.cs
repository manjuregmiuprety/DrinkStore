using DrinkStore.Models;

namespace DrinkStore.Repository
{
    public interface ICustomerRepository
    {
        void Create(Customer customer);
    }
}