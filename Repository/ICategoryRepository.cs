using System.Collections.Generic;
using DrinkStore.Models;

namespace DrinkStore.Repository
{
    public interface ICategoryRepository
    {
        void Create();
        List<Category> GetCategories();
    }
}