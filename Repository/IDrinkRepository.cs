using System.Collections.Generic;
using DrinkStore.Models;

namespace DrinkStore.Repository
{
    public interface IDrinkRepository
    {
        void Create(Drink drink);
        void Update(Drink drink);
        void Delete(Drink drink);
        List<Category> GetAllDrinks();
        Drink GetSingleDrink(int Id);
        List<Category> GetDrinkCategories();
        List<Category> GetDrinkByCategory(string name);
        List<Drink> AddToCart(int Id);
    }
}