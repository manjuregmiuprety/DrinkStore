using System.Collections.Generic;
using DrinkStore.Models;

namespace DrinkStore.Repository
{
    public interface ICartRepository
    {
        void Create(Cart cart);
        void Delete(Cart cart);
        void Update(Cart cart);
        Cart GetSingleCart(int Id);
        List<Cart> GetAllCart();
        List<Cart> GetCustomerCart(string customerId);
        void DeleteCustomerCart(List<Cart> carts);
    }
}