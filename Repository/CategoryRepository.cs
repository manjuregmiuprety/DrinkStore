using System.Collections.Generic;
using System.Linq;
using DrinkStore.Data;
using DrinkStore.Models;

namespace DrinkStore.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private ApplicationDbContext _context;
        public CategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
           
        }

        public List<Category> GetCategories()
        {
            
            return _context.Categories.ToList();
        }
    }
}