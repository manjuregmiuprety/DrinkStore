using System.Collections.Generic;
using System.Linq;
using DrinkStore.Data;
using DrinkStore.Models;
using Microsoft.EntityFrameworkCore;

namespace DrinkStore.Repository
{
    public class CartRepository : ICartRepository
    {
        private ApplicationDbContext _context;
        public CartRepository(ApplicationDbContext context)
        {
            _context = context;
        }
        public void Create(Cart cart)
        {
           _context.Carts.Add(cart);
           _context.SaveChanges();

        }

        public void Delete(Cart cart)
        {
            _context.Carts.Remove(cart);
            _context.SaveChanges();
        }

        public void DeleteCustomerCart(List<Cart> carts)
        {
            _context.Carts.RemoveRange(carts);
            _context.SaveChanges();
        }

        public List<Cart> GetAllCart()
        {
            return _context.Carts.Include(d=>d.Drink)
                                .ToList();
        }

        public List<Cart> GetCustomerCart(string customerId)
        {
            return _context.Carts.Where(c=>c.CustomerId.Equals(customerId))
                                .Include(d=>d.Drink)
                                .ToList();
        }

        public Cart GetSingleCart(int Id)
        {
            return _context.Carts.FirstOrDefault(c=>c.Id==Id);
        }

        public void Update(Cart cart)
        {
            _context.Carts.Update(cart);
            _context.SaveChanges();
        }
    }
}