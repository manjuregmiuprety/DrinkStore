﻿// Write your JavaScript code.
$(function(){

    getcartcount();

    $(".add-to-cart").on("click",function(e){
        var target = e.target;
        var drinkId = $(target).attr("data-drinkId");

        $.ajax({
            url : "/cart/new",
            method : "get",
            data :{drinkId : drinkId},
            success : function(result){
                $("#cartCount").html(result.count + "Items in Cart");
            },
            error : function(error){
                console.log();
            }

        });
    });

    $(".btn-delete").on("click",function(e){
        var target = e.target;
        var cartId = $(target).attr("data-cartId");

        $.ajax({
            url : "/cart/delete",
            merthod : "get",
            data : {cartId : cartId},
            success : function(result){
                $(target).parent("td").parent("tr").fadeOut();
            },
            error : function(error){
                console.log(error);
            }
        })
    });

    function getcartcount(){
        $.ajax({
            url : "/cart/GetCartCount",
            method : "get",
            data :{},
            success : function(result){
                $("#cartCount").html(result.count + "Items in Cart");
            },
            error : function(error){
                console.log();
            }

        });

    }
});
