using System.Collections.Generic;

namespace DrinkStore.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Drink> Drinks { get; set; }
    }
}