namespace DrinkStore.Models
{
    public class Purchase
    {
        public int Id { get; set; }
        public Drink Drink { get; set; }
        public int DrinkId { get; set; }
        public string CustomerId { get; set; }
        public string DrinkName { get; set; }
        public decimal DrinkPrice { get; set; }

    }
}