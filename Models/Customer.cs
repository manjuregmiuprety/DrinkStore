using System.Collections.Generic;

namespace DrinkStore.Models
{
    public class Customer : ApplicationUser
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public List<Cart> Carts { get; set; }

    }
}