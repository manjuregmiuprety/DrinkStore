namespace DrinkStore.Models
{
    public class Cart
    {
        public int Id { get; set; }
        public Drink Drink { get; set; }
        public int DrinkId { get; set; }
        public string CustomerId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

    }
}